# Movie Database
This repository is an induction task for new developers.

You will build a movie database API that exposes its data using GraphQL.

The starting project is configured to use an in-memory database (H2) and will create itself every deploy.

The database structure is discarded when the services are stopped and rebuilt from `src/main/resources/schema.sql` and 
`src/main/resources/data.sql` when started.

To persist data between deployments you may want to configure the project to use PostgreSQL or MySQL.

A [GraphiQL](https://github.com/graphql/graphiql) instance is included in the project and is available at http://localhost:8080,
you can use this to explore and test the API.

## Tasks
1. A movie should have a name (mandatory), a director (mandatory), a list of actors, a release year, age rating, a budget and box office takings. Create entities that model these and their relationships.
2. An actor should have a name (mandatory), a date of birth (mandatory), a nationality (mandatory) and a list of movies they have featured in.
3. A director should have a name (mandatory), a date of birth (mandatory), a nationality (mandatory) and a list of movies they have directed.
4. Add appropriate JSR-303 annotations to mandatory fields.
5. Create Spring Data JPA repositories for the entities created in the previous tasks.
6. There should be GraphQL endpoints for the following for entities created in previous tasks:
    1. Find by id
    2. Find all
    3. Save/Update
    4. Delete
    
## Extra Credit
1. Implement a review system that supports a rating out of 5 and a written review for movies.
2. Provide an endpoint which gives an average rating for a movie based on reviews.
3. Record the actor's role in a movie.
4. Support the uploading of images to represent movies, actors and directors.
