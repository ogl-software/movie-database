FROM maven:3-jdk-11 AS builder
VOLUME $HOME/.m2:/root/.m2
WORKDIR /usr/app
COPY src /usr/app/src
COPY pom.xml /usr/app
RUN mvn clean package -DskipTests

FROM adoptopenjdk/openjdk11:alpine-jre
COPY --from=builder /usr/app/target/*.jar /usr/local/lib/app.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "/usr/local/lib/app.jar"]
