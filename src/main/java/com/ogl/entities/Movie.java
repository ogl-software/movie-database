package com.ogl.entities;

import javax.persistence.Entity;

@Entity
public class Movie extends AbstractEntity {
  private String name;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }
}
