package com.ogl.resolvers;

import com.ogl.entities.Movie;
import com.ogl.repositories.MovieRepository;
import io.leangen.graphql.annotations.GraphQLQuery;
import io.leangen.graphql.spqr.spring.annotations.GraphQLApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@GraphQLApi
@Component
public class MovieResolver {
  private final MovieRepository movieRepository;

  @Autowired
  public MovieResolver(MovieRepository movieRepository) {
    this.movieRepository = movieRepository;
  }

  @GraphQLQuery(name = "movie")
  public Movie findById(Long id) {
    return movieRepository.findById(id).orElseThrow();
  }

  @GraphQLQuery(name = "movies")
  public Iterable<Movie> findAll() {
    return movieRepository.findAll();
  }
}
